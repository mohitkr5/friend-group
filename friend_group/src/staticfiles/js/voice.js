
  // Let's define our first command. First the text we expect, and then the function it should call
  if (annyang) {

    // define the functions our commands will run.
    var hello = function(tag) {
        var u = new SpeechSynthesisUtterance(); 
        var user = document.getElementById('usrnm').value;
     u.text = "Hello!" + user;
     u.lang = 'en-US';
     u.rate = 1.0;
     speechSynthesis.speak(u);
    };
    var contact = function() {
        window.location.href = '/contact/';
     // alert("hello user");
    };
    

    var showsearch = function(tag) {
    $('input[id=search]').val(tag);
        document.getElementById('labnol').submit();
    };
    
    

    var abtus = function() {
      var u = new SpeechSynthesisUtterance(); 
        var user = document.getElementById('usrnm').value;
     u.text = "Hello!" + user;
     u.lang = 'en-US';
     u.rate = 1.0;
     speechSynthesis.speak(u);
      window.location.href = '/about_us/';
    };
    var faq = function() {
      window.location.href = '/faq/';
    };
    var pricing = function() {
      window.location.href = '/pricing/';
    };
    var terms = function() {
      window.location.href = '/terms/';
    };

    var getStarted = function() {
      window.location.href = '/home';
    };

    var closesite = function(){
        var u = new SpeechSynthesisUtterance(); 
        var user = document.getElementById('usrnm').value;
       u.text = "GoodBye!" + user + ' see you soon';
       u.lang = 'hi-IN';
       u.rate = 1.0;
       speechSynthesis.speak(u);
       setTimeout(function(){
        window.location.href = 'https://www.google.co.in/';
       }, 2000)    
    };
    var pagetop = function(){
      document.getElementById('pgtop').click();
    };
    var logout = function(){
        var u = new SpeechSynthesisUtterance(); 
       u.text = "GoodBye! see you soon";
       u.lang = 'hi-IN';
       u.rate = 1.0;
       speechSynthesis.speak(u);
       setTimeout(function(){
          window.location.href = '/account/logout';
       }, 2000)  
    };
    var leaderboard = function(){
        window.location.href = '/home/leaderboard/';
    };
    var searchfrnd = function(){
        window.location.href = '/home/searchusers/';
    };
    var frndrequest = function(){
        window.location.href = '/home/notification/';
    };
    var frndhome = function(){
        window.location.href = '/home/';
    };
    var stop = function(){
       speechSynthesis.cancel();
    };
     var pause = function(){
					speechSynthesis.pause();
    }; 
     var resume = function(){
					speechSynthesis.resume();
    }; 
    
    var uname = function(tag) {
    $('input[id=username]').val(tag);
       // document.getElementById('signinform').submit();
    };
    var loginuname = function(tag) {
    $('input[id=id_username]').val(tag);
       // document.getElementById('signinform').submit();
    };
    var loginpass = function(tag) {
    $('input[id=id_password').val(tag);
        document.getElementById('submit_login_from').click();
    };
    var registerpass = function(tag) {
    $('input[id=pswd]').val(tag);
        document.getElementById('sbmt').click();
    };
    var email = function(tag) {
    $('input[id=email1]').val(tag);
       // document.getElementById('signupform').submit();
    };
    var phone = function(tag) {
    $('input[id=mnum]').val(tag);
      //  document.getElementById('signupform').submit();
    };
    var changegrpname = function(tag) {
    $('input[id=grname]').val(tag);
    document.getElementById('grpsubmit').click();
    };
    var removefrnd = function() {
    document.getElementById('removefrnd').click();
    };
    var accept = function() {
    document.getElementById('accept').click();
    };
    var reject = function() {
    document.getElementById('reject').click();
    };
    var sendrequest = function() {
    document.getElementById('sendrequest').click();
    };
    var postMessage = function(tag){
      $('input[id=id_post]').val(tag);
      document.getElementById('postform').submit();
    };


    // define our commands.
    // * The key is the phrase you want your users to say.
    // * The value is the action to do.
    //   You can pass a function, a function name (as a string), or write your function as part of the commands object.
    var commands = {
      'hello (aptence)':        hello,
      'show (me) *search':      showsearch,
      'search *search':      showsearch,
      'home(page)' :   getStarted,
      'ok I am bored close the site':   closesite,
      'GoodBye (aptence)':   closesite,
      'close the site':   closesite,
      'go to top'  : pagetop,
      'logout' : logout,
      'about us' : abtus,
      'contact(us)' : contact,
      'stop' : stop,
      'chup ho jao' : stop,
      'pause' : pause,
      'resume' : resume,
      'register username *search' : uname,
      'login username *search' : loginuname,
      'Login password *search' : loginpass,
      'Register password *search' : registerpass,
      'email(id) *search' : email,
      'contact number *search' : phone,
      'mobile number *search' : phone,
      'Group Name *search' : changegrpname,
      'Desired Group Name *search' : changegrpname,
      'remove friend' : removefrnd,
      'leaderboard' : leaderboard,
      'find friend' : searchfrnd,
      'Friend Request' : frndrequest,
      'Incoming Request' : frndrequest,
      'Incoming Freind Request' : frndrequest,
      'Notification' : frndrequest,
      'Friend Group' : frndhome,
      'Friend Home' : frndhome,
      'accept' : accept,
      'reject' : reject,
      'send request' : sendrequest,
      'FAQ' : faq,
      'pricing' : pricing,
      'terms' : terms,
      'post *write' : postMessage,
      'write a post *write' : postMessage,

    };

    // OPTIONAL: activate debug mode for detailed logging in the console
    annyang.debug();

    // Add voice commands to respond to
    annyang.addCommands(commands);

    // OPTIONAL: Set a language for speech recognition (defaults to English)
    // For a full list of language codes, see the documentation:
    // https://github.com/TalAter/annyang/blob/master/docs/FAQ.md#what-languages-are-supported
    annyang.setLanguage('en');

    // Start listening. You can call this here, or attach this call to an event, button, etc.
   annyang.start({ continuous: false });
  } else {
    $(document).ready(function() {
      $('#unsupported').fadeIn('fast');
    });
  }

  var scrollTo = function(identifier, speed) {
    $('html, body').animate({
        scrollTop: $(identifier).offset().top
    }, speed || 1000);
  }
