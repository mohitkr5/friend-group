# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render, redirect
from django.urls import reverse

from accounts.forms import (
    RegistrationForm,
    EditProfileForm,
    EditPersonalDetailsForm
)

from django.contrib.auth.models import User
from django.contrib.auth.forms import UserChangeForm, PasswordChangeForm
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q
from urllib import quote_plus
from home.models import Friend, FriendRequest
from .models import UserProfile


def register(request):
    if request.method =='POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            user = form.save()
            # friend_cls_obj = Friend.objects.create(current_user=user)
            #userpro = UserProfile.objects.create(user=user)
            #friend_cls_obj.friends.add(userpro)
            # FriendRequest.objects.create(receiver=user)
            return redirect(reverse('accounts:login'))
    else:
        form = RegistrationForm()

        args = {'form': form}
        return render(request, 'accounts/reg_form.html', args)

def view_profile(request, pk=None):
    if pk:
        user = UserProfile.objects.get(pk=pk)
    else:
        user = request.user.userprofile
    args = {'user': user}
    return render(request, 'accounts/profile.html', args)

def edit_profile(request):
    if request.method == 'POST':
        form = EditProfileForm(request.POST, instance=request.user)
        form1 = EditPersonalDetailsForm(request.POST or None, request.FILES or None, instance=request.user.userprofile)
        ds = request.POST['description']
        ct = request.POST['city']
        ws = request.POST['website']
        ph = request.POST['phone']
        print(ds, ph, ct, ws)
        if form.is_valid() and form1.is_valid():
            form.save()
            if request.POST['image'] is not '':
                request.user.userprofile.image = "profile_image/"+request.POST['image']
                print("bb",request.POST['image'])
                request.user.userprofile.save()
            else:
                print("aa",request.user.userprofile.image.url)
            form1.save()
            return redirect(reverse('accounts:view_profile'))
    else:
        form = EditProfileForm(instance=request.user)
        form1 = EditPersonalDetailsForm(instance=request.user.userprofile)
        args = {'form': form, 'form1': form1,}
        return render(request, 'accounts/edit_profile.html', args)

def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(data=request.POST, user=request.user)

        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)
            return redirect(reverse('accounts:view_profile'))
        else:
            return redirect(reverse('accounts:change_password'))
    else:
        form = PasswordChangeForm(user=request.user)

        args = {'form': form}
        return render(request, 'accounts/change_password.html', args)

