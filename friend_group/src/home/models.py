# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from accounts.models import UserProfile

class Post(models.Model):
    post = models.CharField(max_length=500)
    user = models.ForeignKey(User)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.user.username + " : " + self.post


class Friend(models.Model):
    friends = models.ManyToManyField(UserProfile) 
    current_user = models.ForeignKey(User, related_name='owner', null=True)
    rank = models.IntegerField(default='0')
    group_name = models.CharField(max_length=20, default="Group", help_text="Group name must not be greater than 20 characters")


    def make_friend(self, new_friend):
        self.friends.add(new_friend)

    def lose_friend(self, friend_to_be_removed):
        self.friends.remove(friend_to_be_removed)

    def __str__(self):
        return self.current_user.username


class FriendRequest(models.Model):
    sender = models.ManyToManyField(UserProfile) 
    receiver = models.ForeignKey(User, related_name='receiver', null=True)
    notification = models.CharField(max_length=300)

    def __str__(self):
        return self.receiver.username

def initializer_friends_related_objects(sender, instance, **kwargs):

    if kwargs.get('created'):
        friend_cls_obj = Friend.objects.create(current_user=instance)
        userpro = UserProfile.objects.create(user=instance)
        friend_cls_obj.friends.add(userpro)
        FriendRequest.objects.create(receiver=instance)

models.signals.post_save.connect(initializer_friends_related_objects, sender=User)