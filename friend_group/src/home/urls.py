from django.conf.urls import url
from home.views import HomeView
from . import views
from .views import users_list, leaderboard, friend_request, notification, resolve_request, change_group_name

urlpatterns = [
    url(r'^$', HomeView.as_view(), name='home'),
    url(r'^connect/(?P<operation>.+)/(?P<pk>\d+)/$', views.change_friends, name='change_friends'),
    url(r'^searchusers/$', users_list, name='list'),
    url(r'^leaderboard/$', leaderboard, name='leaderboard'),
    url(r'^request/(?P<pk>\d+)/$', friend_request, name='send_request'),
    url(r'^resolverequest/(?P<operation>.+)/(?P<pk>\d+)/$', resolve_request, name='resolve_request'),
    url(r'^notification/$', notification, name='notification'),
    url(r'^change_group_name/$', change_group_name, name='change_group_name'),
]
