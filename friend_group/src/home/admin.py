# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from home.models import Post, Friend, FriendRequest

class FriendProfileAdmin(admin.ModelAdmin):
    list_display = ('current_user', 'group_name')


admin.site.register(Post)
admin.site.register(FriendRequest)
admin.site.register(Friend, FriendProfileAdmin)
