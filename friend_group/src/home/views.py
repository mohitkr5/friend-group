# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.views.generic import TemplateView
from django.shortcuts import render, redirect
from django.contrib.auth.models import User

from home.forms import HomeForm
from home.models import Post, Friend, FriendRequest
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q
from urllib import quote_plus
from utils import Rank
from accounts.models import UserProfile


class HomeView(TemplateView):
    template_name = 'home.html'

    def get(self, request):
        rank_obj = Rank(request.user)
        form = HomeForm()
        posts = Post.objects.all().order_by('-created')
        users = UserProfile.objects.exclude(user=request.user.id)
        user = Friend.objects.get(current_user=request.user)
        friend = user.friends.exclude(user=request.user.id)
        paginator = Paginator(posts, 10) # Show 10 records per page
        page_request_var = "page"
        page = request.GET.get(page_request_var)
        try:
            posts = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            posts = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            posts = paginator.page(paginator.num_pages)

        args = {
            'form': form, 'posts': posts, 'users': users, 'friends': friend, 'frndobj': user, 'rank_obj': rank_obj.rank(),
            'page_request_var' : page_request_var,
        }
        return render(request, self.template_name, args)

    def post(self, request):
        form = HomeForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.user = request.user
            post.save()

            text = form.cleaned_data['post']
            form = HomeForm()
            return redirect('home:home')

        args = {'form': form, 'text': text}
        return render(request, self.template_name, args)

    

def change_friends(request, operation, pk):
    friend = UserProfile.objects.get(pk=pk)
    currentuser = UserProfile.objects.get(user=request.user)
    friend_cls_obj = Friend.objects.get(current_user=request.user)
    opposite_friend = Friend.objects.get(current_user=friend.user)
    if operation == 'add':
        friend_cls_obj.make_friend(friend)
    elif operation == 'remove':
        friend_cls_obj.lose_friend(friend)
        opposite_friend.lose_friend(currentuser)
    return redirect('home:home')

def users_list(request):
        #today = timezone.now()
        queryset_list = UserProfile.objects.exclude(user=request.user.id) #filter(draft=False).filter(publish__lte=timezone.now()) #.all() #.order_by("-timestamp")
        user = Friend.objects.get(current_user=request.user)
        print user
        friend = user.friends.exclude(user=request.user.id)
        print friend
        cur_user = UserProfile.objects.get(user=request.user)
        current_user_sent_request = FriendRequest.objects.filter(sender=cur_user).values_list('receiver__userprofile', flat=True)
        current_user_sent_request_username = FriendRequest.objects.filter(sender=cur_user).values_list('receiver__userprofile__user__username', flat=True)
        print current_user_sent_request_username
        friend_request = FriendRequest.objects.get(receiver=request.user)
        print 'friend request', friend_request
        cur_user_incoming_request_list = friend_request.sender.exclude(user=request.user)
        print '525', cur_user_incoming_request_list
        #print friend_request
        if request.user.is_staff or request.user.is_superuser:
            queryset_list = UserProfile.objects.exclude(user=request.user.id)
    
        query = request.GET.get("q")
        if query:
            queryset_list = queryset_list.filter(
                Q(user__username__icontains=query)|
                Q(user__first_name__icontains=query)|
                Q(user__last_name__icontains=query)|
                Q(city__icontains=query)|
                Q(phone__icontains=query)
                ).distinct()
        paginator = Paginator(queryset_list, 10) # Show 10 records per page
        page_request_var = "page"
        page = request.GET.get(page_request_var)
        try:
            queryset = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            queryset = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            queryset = paginator.page(paginator.num_pages)
        context = {
            "object_list" : queryset,
            "title" : "Aptence User List",
            "page_request_var" : page_request_var,
            "friends" : friend,
            "my_sent_request" : current_user_sent_request,
            "my_sent_request_username" : current_user_sent_request_username,
            "incoming_request" : cur_user_incoming_request_list,
        }

        return render(request, "users_list.html" ,context)

def leaderboard(request):
    rank_obj = Rank(request.user)
    #users = UserProfile.objects.all().order_by('coin')
    user = Friend.objects.get(current_user=request.user)
    lbusers_list = user.friends.all().order_by('-coin')
    print lbusers_list
    context = {
        'rank_obj': rank_obj.rank(),
        'lbusers_list': lbusers_list,
    }

    return render(request, 'leaderboard.html', context)

def friend_request(request, pk):
    #sender_obj = FriendRequest.objects.get_or_create(sender=request.user)
    friend = UserProfile.objects.get(pk=pk)
    sender_obj = UserProfile.objects.get(user=request.user)
    print friend
    #print request.user
    receiver_obj = FriendRequest.objects.get_or_create(receiver=friend.user)
    receiver_obj[0].sender.add(sender_obj) 
    #receiver_obj.receiver= friend.user.username
    receiver_obj[0].save()
    friend.notification = "You received a friend request"
    friend.save()
    print receiver_obj
    print friend
    return redirect('home:list')
    
def notification(request):
    user = FriendRequest.objects.get(receiver=request.user)
    lbusers_list = user.sender.all()
    context = {
        'currentuser': user,
        'lbusers_list': lbusers_list,
    }

    return render(request, 'notification.html', context)

def resolve_request(request, operation, pk):
    friend = UserProfile.objects.get(pk=pk)
    receiver_obj = FriendRequest.objects.get(receiver= request.user)
    if operation == "accept":
        #add sender in receivers friend list
        friend_cls_obj = Friend.objects.get(current_user=request.user)
        friend_cls_obj.make_friend(friend)
        #add receiver in senders friend list
        sender = UserProfile.objects.get(user= request.user)
        sender_obj = Friend.objects.get(current_user= friend.user)
        sender_obj.make_friend(sender)
        receiver_obj.sender.remove(friend)
    elif operation == "reject":
        receiver_obj.sender.remove(friend)
    return redirect('home:notification')

def change_group_name(request):
    friend_cls_obj = Friend.objects.get(current_user=request.user)
    grpname = request.POST['grpname']
    print grpname
    friend_cls_obj.group_name = grpname
    friend_cls_obj.save()
    return redirect('home:home')
