# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-10-13 09:48
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0005_remove_friend_rank'),
    ]

    operations = [
        migrations.AddField(
            model_name='friend',
            name='rank',
            field=models.IntegerField(default='0'),
        ),
    ]
