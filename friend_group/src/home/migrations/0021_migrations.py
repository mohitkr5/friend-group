from __future__ import unicode_literals

from django.db import migrations

def forwards_func(apps, schema_editor):
    # We get the model from the versioned app registry;
    # if we directly import it, it'll be the wrong version
    Friend = apps.get_model("home", "Friend")
    FriendRequest = apps.get_model("home", "FriendRequest")
    User = apps.get_model("auth", "User")
    #db_alias = schema_editor.connection.alias
    for user in User.objects.all():
        if not user.owner.all().exists():
            Friend.objects.create(current_user=user)
    for user in User.objects.all():
        if not user.receiver.all().exists():
            FriendRequest.objects.create(receiver=user)


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0020_auto_20171031_0548'),
    ]

    operations = [
        migrations.RunPython(forwards_func),
    ]